from django.urls import path, re_path
from .views import home, article_detail

app_name = "blog2"

urlpatterns = [
    path('home', home, name='home'),
    path('article/<slug:slug>', article_detail, name='article_detail')
]