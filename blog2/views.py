from django.shortcuts import render

from blog.models import Article


def home(request):
    context = {
        "articles": Article.objects.filter(status='p').order_by("-publish")[:3]
    }
    return render(request, "home.html", context)


def article_detail(request, slug):
    context = {
        "article": Article.objects.get(slug=slug)
    }
    return render(request, "article_detail.html", context)

# Create your views here.
