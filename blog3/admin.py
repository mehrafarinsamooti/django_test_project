from django.contrib import admin
from django.contrib import messages
from django.utils.translation import ngettext
from .models import Article, Category

# Register your models here.

# admin.site.disable_action('delete_selected')

admin.site.site_header = "وبلاگ مهرآفرین"


@admin.action(description='انتشار مقالات انتخاب شده')
def make_published(modeladmin, request, queryset):
    updated = queryset.update(status='p')
    modeladmin.message_user(request, ngettext(
        '%d مقاله با موفقیت منتشر شد',
        '%d مقاله با موفقیت منتشر شدند',
        updated,
    ) % updated, messages.SUCCESS)


@admin.action(description='پیش‌نویس مقالات انتخاب شده')
def make_draft(modeladmin, request, queryset):
    updated = queryset.update(status='d')
    modeladmin.message_user(request, ngettext(
        '%d مقاله با موفقیت پیش‌نویس شد',
        '%d مقاله با موفقیت پیش‌نویس شدند',
        updated,
    ) % updated, messages.SUCCESS)


@admin.action(description='غیرفعال کردن دسته‌بندی‌های انتخاب شده')
def make_inactive(modeladmin, request, queryset):
    updated = queryset.update(status=False)
    modeladmin.message_user(request, ngettext(
        '%d دسته‌بندی با موفقیت غیرفعال شد',
        '%d دسته‌بندی با موفقیت غیرفعال شدند',
        updated,
    ) % updated, messages.SUCCESS)


@admin.action(description='فعال کردن دسته‌بندی‌های انتخاب شده')
def make_active(modeladmin, request, queryset):
    updated = queryset.update(status=True)
    modeladmin.message_user(request, ngettext(
        '%d دسته‌بندی با موفقیت فعال شد',
        '%d دسته‌بندی با موفقیت فعال شدند',
        updated,
    ) % updated, messages.SUCCESS)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('position', 'title', 'slug', 'parent', 'status')
    list_filter = ('status',)
    search_fields = ('title', 'slug')
    prepopulated_fields = {'slug': ('title',)}
    actions = [make_inactive, make_active]


admin.site.register(Category, CategoryAdmin)


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'thumbnail_tag', 'slug', 'jalali_publish', 'status', 'category_to_str', 'author')
    list_filter = ('status', 'publish', 'author')
    search_fields = ('title', 'description')
    prepopulated_fields = {'slug': ('title',)}
    # ordering = ('-status', 'publish')
    actions = [make_published, make_draft]


admin.site.register(Article, ArticleAdmin)


