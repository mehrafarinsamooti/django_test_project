from django.urls import path
from .views import ArticleDetail, ArticleList, CategoryList, AuthorList

app_name = "blog3"

urlpatterns = [
    path('blog3/home', ArticleList.as_view(), name='home'),
    path('blog3/home/page/<int:page>', ArticleList.as_view(), name='home'),
    path('blog3/detail.html/<slug:slug>', ArticleDetail.as_view(), name='article_details'),
    path('blog3/category/<slug:slug>', CategoryList.as_view(), name='category'),
    path('blog3/category/<slug:slug>/page/<int:page>', CategoryList.as_view(), name='category'),
    path('blog3/author/<slug:username>', AuthorList.as_view(), name='author'),
    path('blog3/author/<slug:username>/page/<int:page>', AuthorList.as_view(), name='author')
]
