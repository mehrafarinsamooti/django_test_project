from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, JsonResponse
from .models import Category, Article
from django.core.paginator import Paginator
from django.views.generic import ListView, DetailView


# def home(request, page=1):
#     # context = {
#     #     "articles": Article.objects.filter(status='p')
#     # }
#     # context = {
#     #     "articles": Article.objects.published()
#     # }
#     articles = Article.objects.published()
#     paginator = Paginator(articles, 2)
#     # page_number = request.GET.get('page')
#     page_obj = paginator.get_page(page)
#     return render(request, "blog3/home.html", {"articles": page_obj})

class ArticleList(ListView):
    # model = Article
    # template_name = "blog3/article_list.html"
    # context_object_name = "object_list"
    queryset = Article.objects.published()
    paginate_by = 2


# def article_details(request, slug='Kentia_Palm_Plant'):
#     # try:
#     #     article = Article.objects.get(slug=slug)
#     # except Exception as e:
#     #     raise Http404
#     context = {
#         # "article": get_object_or_404(Article, slug=slug, status='p')
#         "article": get_object_or_404(Article.objects.published(), slug=slug)
#     }
#     return render(request, 'blog3/detail.html', context)


class ArticleDetail(DetailView):
    def get_object(self):
        slug = self.kwargs.get('slug')
        return get_object_or_404(Article.objects.published(), slug=slug)


# def category(request, slug, page=1):
#     category = get_object_or_404(Category, slug=slug, status=True)
#     articles = category.articles.published()
#     paginator = Paginator(articles, 2)
#     # page = request.GET.get('page')
#     articles = paginator.get_page(page)
#     context = {
#         # "category": get_object_or_404(Category, slug=slug, status=True)
#         # get_object_or_404(Category.objects.active_category(), slug=slug)
#         "category": category,
#         "articles": articles
#     }
#     return render(request, 'blog3/category_list.html', context)


class CategoryList(ListView):
    template_name = 'blog3/category_list.html'
    paginate_by = 2

    def get_queryset(self):
        global category
        slug = self.kwargs.get('slug')
        category = get_object_or_404(Category.objects.active_category(), slug=slug)
        return category.articles.published()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = category
        return context

# class CategoryList(ListView):
#     paginate_by = 2
#     template_name = 'blog3/category_list.html'
#
#     def get_queryset(self):
#         slug = self.kwargs.get('slug')
#         category = get_object_or_404(Category.objects.active_category(), slug=slug)
#         return category.articles.published()


class AuthorList(ListView):
    template_name = 'blog3/author_list.html'
    paginate_by = 2

    def get_queryset(self):
        global author
        username = self.kwargs.get('username')
        author = get_object_or_404(User, username=username)
        return author.articles.published()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['author'] = author
        return context


