from . import jalali
from django.utils import timezone

jalai_month = {
    "1": "فروردین",
    "2": "اردیبهشت",
    "3": "خرداد",
    "4": "تیر",
    "5": "مرداد",
    "6": "شهریور",
    "7": "مهر",
    "8": "آبان",
    "9": "آذر",
    "10": "دی",
    "11": "بهمن",
    "12": "اسفند",
}


def persian_number_convertor(mystr):
    number_convertor = {
        "0": "۰",
        "1": "۱",
        "2": "۲",
        "3": "۳",
        "4": "۴",
        "5": "۵",
        "6": "۶",
        "7": "۷",
        "8": "۸",
        "9": "۹"
    }
    for e, p in number_convertor.items():
        mystr = mystr.replace(e, p)
    return mystr


def jalali_convertor(time):
    time = timezone.localtime(time)
    time_to_str = f"{time.year}-{time.month}-{time.day}"
    jalali_time = jalali.Gregorian(time_to_str).persian_tuple()
    # output = f"{jalali_time[2]} {jalai_month[str(jalali_time[1])]} {jalali_time[0]},  ساعت{time.hour}:{time.minute} "
    output = "{} {} {} , ساعت {}:{}".format(
        jalali_time[2],
        jalai_month[str(jalali_time[1])],
        jalali_time[0],
        time.hour,
        time.minute
    )
    return persian_number_convertor(output)
