from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import Article

# Create your views here.


def home(request):
    context = {
        "products": [
            {
                "title": "پد روزانه تافته مدل UltraThin بسته 40 عددی ",
                "description": "this is refunable",
                "img": "https://dkstatics-public.digikala.com/digikala-products/112027252.jpg?x-oss-process=image/resize,m_lfit,h_115,w_115/quality,q_60"
            },
            {
                "title": "ارسال توسط فروشنده",
                "description": "this is refunable",
                "img": "https://dkstatics-public.digikala.com/digikala-adservice-banners/6242330641b83b4bb5a4c13ff85174b48b644432_1628323882.jpg?x-oss-process=image/quality,q_80"
            },
            {
                "title": "محصولات بومی و محلی ",
                "description": "this is refunable",
                "img": "https://dkstatics-public.digikala.com/digikala-adservice-banners/956cd52f1f18f11284016c86561d53bcdcfdeedd_1612606849.jpg?x-oss-process=image/quality,q_80"
            }
        ]
    }
    return render(request, "blog/home.html", context)
    # return HttpResponse("Hello world!, Hello django!")


def api(request):
    data = {
        "1": {
            "name": "مقاله ی اول",
            "id": "1",
            "slug": "first article"
        },
        "2": {
            "name": "مقاله ی دوم",
            "id": "2",
            "slug": "second article"
        },
        "3": {
            "name": "مقاله ی سوم",
            "id": "3",
            "slug": "third article"
        }

    }
    return JsonResponse(data)
